#include "Frontend.h"

int main() {

    Frontend frontend = Frontend();
    frontend.welcomeTheUser();
    frontend.parseInputAndDisplay();

    return 0;
}
