
class Backend {
private:
    void checkIntegers(int a, int b);

public:
    int add(int a, int b);
    int multiply(int a, int b);

};