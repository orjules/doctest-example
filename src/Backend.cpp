#include "Backend.h"
#include "stdexcept"

void Backend::checkIntegers(int a, int b) {
    if (a<0 || b<0){
        throw std::invalid_argument("The numbers must be positive!");
    }
}

int Backend::add(int a, int b) {
    checkIntegers(a, b);
    return a + b;
}

int Backend::multiply(int a, int b) {
    checkIntegers(a, b);
    return a * b;
}