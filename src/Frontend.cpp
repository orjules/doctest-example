#include <iostream>
#include <stdexcept>
#include "Frontend.h"

using namespace std;

void Frontend::welcomeTheUser() {
    cout << "Welcome user!" << endl;
    cout << "In this program you can do two things. Either number 'number + number' oder 'number + number'." << endl;
    cout << "In both cases 'number' must be an unsigned integer. Note that comma separation is important." << endl;
}

void Frontend::parseInputAndDisplay() {
    cout << endl;
    cout << "Enter the first number: " << endl;
    int firstNumber;
    cin >> firstNumber;

    cout << "Now enter either '+' or '*': " << endl;
    char operation;
    cin >> operation;

    cout << "Lastly, enter a second number: " << endl;
    int secondNumber;
    cin >> secondNumber;

    int resultNumber;

    if (operation == '+'){
        resultNumber = backend.add(firstNumber, secondNumber);
    }else if(operation == '*'){
        resultNumber = backend.multiply(firstNumber, secondNumber);
    }else{
        throw invalid_argument("String between numbers must be either '+' or '*'!");
    }

    cout << "The result is: " << resultNumber << endl;
}