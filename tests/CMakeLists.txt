cmake_minimum_required(VERSION 3.22)
project(Tests)

set(CMAKE_CXX_STANDARD 14)

add_executable(${PROJECT_NAME} ../src/Backend.cpp test_backend.cpp test_other.cpp)