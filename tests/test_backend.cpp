#include "../src/Backend.h"
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"


TEST_CASE("Addition"){
    Backend b;
    CHECK(b.add(0,0) == 0);
    CHECK(b.add(0,1) == 1);
    CHECK(b.add(1,0) == 1);
    CHECK(b.add(2,3) == 5);
}

TEST_CASE("Multiplication"){
    Backend b;
    CHECK(b.multiply(0,0) == 0);
    CHECK(b.multiply(0,1) == 0);
    CHECK(b.multiply(1,0) == 0);
    CHECK(b.multiply(1,1) == 1);
}

TEST_CASE("Errors"){
    Backend b;
    CHECK_THROWS(b.add(-1, 0));
    CHECK_THROWS(b.multiply(-1, 0));
}