# Testing of the testing framework Doctest

For the upcoming rover project Taygeta I would like to have a simple way to write Unit Tests.
A quick Google search gave the [doctest framework](https://github.com/doctest/doctest), which is really very simple.

## Installation and setup

The whole test framework is contained in a single header file. 
The [raw version](https://raw.githubusercontent.com/doctest/doctest/master/doctest/doctest.h) can just be copied and 
pasted into a file named `doctest.h`.

Before doing so a certain directory structure should be made:

```
\-- root
    \-- CMakeLists.txt.
    \-- src
        \-- CMakeLists.txt.
        \-- main.cpp
        \-- classA.cpp
        \-- ...
    \-- tests
        \-- CMakeLists.txt.
        \-- test_classA.cpp
        \-- ...
```

The `CMakeLists.txt` in `src` should have a project name like `Run` and that in `tests` something like `Tests`.
Notable for the `Tests` project is that the executables (.ccp files) that are being tested, need to be added.
The top-level `CMakeLists.txt` simply combines the other two by using `add_subdirectory(src)` and 
`add_subdirectory(tests)`. Examples can be found in this repo.

With this setup (and after rerunning CMAKE) it is possible in CLion to change the Configurations like so:

![Image of the changing of configurations](static/ChangingConfigExample.png)

CLion also has integration fοr this framework and will show a nice GUI when running the tests.

## Writing tests

To make a file into a test file two things are needed:
```
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
```

**IMPORTANT:** The `#define` creates its own main function, so it can only be used once per `CMakeLists.txt` project.
This is shown in the two files `test_backend.cpp` and `test_other.cpp` where both can `#include "doctest"` but only one
has the `#define` **in front** of it.

That is the whole reason for the setup as shown above. 
It allows for the `src` directory to have its own `main` function.

When the defines and includes are done, actual tests can be written.
A testcase is defined with a macro and given a name to find it in case of an error.
Here is an example which shows both the checking of a boolean expression, and looking for thrown errors.

```
TEST_CASE("Addition"){
    Backend b;
    CHECK(b.add(0,0) == 0);
    CHECK_THROWS(b.add(-1, 0));
}
```

More information can be found on the [GitHub page](https://github.com/doctest/doctest) of the project where you can 
find a written tutorial and code examples.
